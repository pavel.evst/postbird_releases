const path = require('path');
const domain = require('domain');

module.exports.reqDomain = function (req, res, next) {
  var reqDomain = domain.create();
  reqDomain.request = req;

  req.errorData = {};

  reqDomain.add(req);
  reqDomain.add(res);

  reqDomain.on('error', function(err) {
    console.error(err);

    if (!res.headersSent) {
      res.status(500);
    }

    if (!res.finished) {
      console.log("Exception happen, render error page");
      //reportError(err, req);
      res.render('error', {
        message: err.message,
        error: {}
      });
    }
  });

  reqDomain.run(function() {
    next();
  });
};

module.exports.sass = function () {
  var sassMiddleware = require('node-sass-middleware');
  return sassMiddleware({
    src: path.join(__dirname, '../public'),
    dest: path.join(__dirname, '../public'),
    indentedSyntax: false, // true = .sass and false = .scss
    sourceMap: true,
    outputStyle: 'compressed'
  });
}

// eslint-disable-next-line no-unused-vars
module.exports.errorHandler = function(err, req, res, next) {
  console.log('error handler');
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  console.error(err.message + "\n" + err.stack);

  // render the error page
  res.status(err.status || 500);
  res.render('error');

  /*
  if (err instanceof Errors.AuthError) {
    req.flash('error', err.message);
    res.render('auth_error');
  } else if (err instanceof Errors.Unauthorized) {
    req.flash('error', err.message);
    res.redirect('/');
  } else {
    reportError(err, req);
  }
  */
};