const methods = require('methods');

const catchAsyncErrors = (fn) => {
  return (req, res, next) => {
    const routePromise = fn(req, res, next);
    if (routePromise && routePromise.catch) {
      routePromise.catch(err => {
        next(err)
      });
    }
  }
}

const flatten = (list) => {
  return list.reduce((a, b) => {
    return a.concat(Array.isArray(b) ? flatten(b) : b);
  }, []);
};

const slice = Array.prototype.slice;

var asyncRouter = (router) => {
  methods.concat('all').forEach(method => {
    var original = router[method];
    router[method] = function (path) {
      var handles = flatten(slice.call(arguments, 1));
      var patched = [];

      handles.forEach(handle => {
        if (typeof handle !== 'function') {
          var type = toString.call(handle);
          var msg = 'Route.all() requires a callback function but got a ' + type
          throw new TypeError(msg);
        }

        patched.push(catchAsyncErrors(handle));
      });

      original.call(this, path, patched);
    }
  });

  return router;
};

module.exports = asyncRouter;