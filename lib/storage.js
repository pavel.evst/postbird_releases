const webdav = require("webdav");
const path = require("path");
const axios = require("axios");
const hyperquest = require('hyperquest');

class Storage {
  constructor() {
    this.server   = process.env.WEBDAV_SERVER;
    this.user     = process.env.WEBDAV_USER;
    this.password = process.env.WEBDAV_PASSWORD;
    this.prefix   = process.env.WEBDAV_PREFIX;

    if (!this.server) {
      console.error("Missing env variable WEBDAV_SERVER");
      process.exit(1)
    }

    this.client = webdav.createClient(this.server, {
      username: this.user,
      password: this.password
    });
  }

  ls(dir = "") {
    var davPath = path.join(this.prefix, dir);
    console.log("WEBDAV ls", davPath);
    return this.client.getDirectoryContents(davPath);
  }

  stat(file) {
    var davPath = path.join(this.prefix, file);
    return this.client.stat(davPath);
  }

  createReadStream(remotePath, options = {}) {
    var davPath = path.join(this.prefix, remotePath);
    var url = this.client.getFileDownloadLink(davPath);

    return axios({
      method: 'get',
      url: url,
      headers: options.headers,
      responseType: 'stream'
    })
  }

  async createWriteStream(remotePath) {

    // remove leading zero
    if (remotePath.startsWith('/')) {
      remotePath = remotePath.replace('/', '');
    }
    remotePath = remotePath.replace(/\/\//g, '/'); // replace double slashes with single

    var davPath = path.join(this.prefix, remotePath);

    var pathParts = davPath.split("/").slice(0, -1);

    var lastDir = '';
    for (let part of pathParts) {
      if (part == '') {
        continue;
      }

      var dirPath = path.join(lastDir, part);
      lastDir = dirPath;

      try {
        console.log('WEBDAV mkdir', dirPath)
        await this.client.createDirectory(dirPath);
        console.log(`Create ${dirPath} OK`);
      } catch (error) {
        console.log('error', error.message)
        console.log(error);
      }
    }

    // use hyperquest because webdav uses axios which uses follow-redirects which limit request body
    // so all of them kinda suck in this case
    var uploadUrl = this.client.getFileUploadLink(davPath);

    var stream = hyperquest.put(uploadUrl);

    return stream;
  }
}

module.exports = Storage;
