module.exports = {
    "env": {
        "node": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "indent": [
          "warn", 2,
          {ignoreComments: true}
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "off",
            "double"
        ],
        "semi": [
            "off",
            "never"
        ],
        "no-console": "off",
        "no-useless-escape": "off",
        "no-octal": "off",
        "no-constant-condition": [
          "error", { "checkLoops": false }
        ]
    }
};