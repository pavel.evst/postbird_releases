const express = require('express');
const morgan = require('morgan');
const addRequestId = require('express-request-id')({setHeader: false});
const path = require('path');
const middlewares = require('./lib/app_middlewares');
const asyncRouter = require('./lib/async_router');
const Storage = require('./lib/storage');

var app = express();

morgan.token('id', (req) => req.id.split('-')[0])

app.use(express.static(path.join(__dirname, 'public')));

app.use(addRequestId);
app.use(morgan("[:date[iso] #:id] Started :method :url for :remote-addr", {immediate: true}));
app.use(morgan("[:date[iso] #:id] Completed :status :res[content-length] in :response-time ms"));

/*
if (app.get('env') == "development") {
  app.use(morgan('dev'));
} else {
  app.use(morgan('common'));
}
*/


app.use(middlewares.reqDomain);

app.locals = Object.assign(
  app.locals,
  {req: {}},
  { // helpers
    fileSize(size) {
      var e = (Math.log(size) / Math.log(1e3)) | 0;
      return +(size / Math.pow(1e3, e)).toFixed(2) + ' ' + ('kMGTPEZY'[e - 1] || '') + 'B';
    }
  }
  //require('./lib/view_helpers')
);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

if (app.get('env') == "development") {
  app.use(middlewares.sass());
}

app.set('trust proxy', 1);

app.use(function (req, res, next) {
  res.locals.req = req;
  res.locals.res = res;
  return next();
});

var router = asyncRouter(express.Router());

var storage = new Storage();

router.get('*', async (req, res) => {
  var breadcrambs = [];
  req.path.split("/").forEach(part => {
    if (part != '') {
      var prefix = breadcrambs.length > 0 ? breadcrambs[breadcrambs.length - 1].url : "/";
      breadcrambs.push({
        url: path.join(prefix, part),
        title: part
      });
    }
  });
  if (breadcrambs.length) {
    breadcrambs[breadcrambs.length - 1].current = true;
  }

  if (req.query.dl == '1') {
    var reqHeaders = {};
    if (req.headers.range) {
      reqHeaders.range = req.headers.range
    }

    var response = await storage.createReadStream(req.path, {headers: reqHeaders});

    console.log('response headers', response.headers);

    // copy headers
    res.status(response.status);
    for (let header of Object.keys(response.headers)) {
      if (header != 'date' && header != 'server' && header != 'etag' && header != 'connection') {
        res.set(header, response.headers[header]);
      }
    }
    if (!response.headers['content-disposition']) {
      res.set('content-disposition', `attachment; filename="${path.basename(req.path)}"`);
    }

    response.data.pipe(res);
  } else {
    var files = [];
    try {
      files = await storage.ls(req.path);
      files = files.sort((a, b) => {
        return a.basename.localeCompare(b.basename);
      });
    } catch (error) {
      console.dir(error);
      throw error;
    }

    console.log(`Remote objects: ${files.map(f => f.basename).join(', ')}`);
    res.render('index', {breadcrambs, files});
  }
})

router.put('*', async (req, res) => {
  var uploadPath = req.path;
  console.log(`Trying to save file: ${uploadPath}`);

  if (!process.env.UPLOAD_TOKEN || process.env.UPLOAD_TOKEN == '') {
    console.log("process.env.UPLOAD_TOKEN is missing");
  }

  if (req.get("upload-token") != process.env.UPLOAD_TOKEN) {
    console.log(`invalid upload token: '${req.get("upload-token")}' != '${process.env.UPLOAD_TOKEN}'`);
    res.status(403);
    res.send(`Error: Upload Token is invalid`);
    return;
  }

  var fileExists = false;
  try {
    var stat = await storage.stat(uploadPath);
    console.log(stat);

    fileExists = true;

  } catch (error) {
    if (error.message == 'Request failed with status code 404') {
      console.log('file not exist, good');
    } else {
      throw error;
    }
  }

  if (fileExists) {
    console.log('file already exists');
    res.status(422);
    res.send(`Error: File ${uploadPath} already exists`);
    return;
  }

  console.log(`Saving file to webdav - ${uploadPath}`);
  var writeStream = await storage.createWriteStream(uploadPath);

  console.log(`Streaming file content to webdav - ${uploadPath}`);
  req.pipe(writeStream);

  req.on('end', () => {
    res.send('File saved');
  });
});

app.use('/', router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  if (app.get('env') == "development") {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else {
    res.status(404);
    res.render("errors/404");
  }
});

// error handler
app.use(middlewares.errorHandler);

module.exports = app;
