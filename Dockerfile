FROM node:10-alpine

RUN apk update && apk upgrade && \
    apk add bash curl openssl && \
    rm -rf /var/cache/apk/*

RUN mkdir -p /opt/app
WORKDIR /opt/app

COPY package.json yarn.lock /opt/app/

RUN yarn install --production

COPY . /opt/app/

ENV NODE_ENV production

EXPOSE 3000
CMD node bin/app
